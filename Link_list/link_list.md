# Linked list
> Linked lists are composed of nodes and references/pointers pointing from one node to the other!!!

The last reference is pointing to a NULL.

## A single node 

- contains data -> integer,double or custom object
- contains a reference pointing to the next node in the linked list

    
    class Node {
        data
        Node nextNode
        ...
    }
    
- Each node is composed of a data and a reference/link to the next node in the sequence
- Simple and very common data structure!!!
- They can be used to implement several other common data types:stacks,queues
- Simple linked lists by themselves do not allow random access to the data 
- Many basic operations such as obtaining the last node of the list or finding a node that contains a given
data or locating the place where a new node should be inserted -- require sequential scanning 
of most or all of the list elements
