# linear structures

# Stacks

- A stack is an ordered collection of items where the addition of new items and 
the removal of existing items always takes place at the same end.

- This end is commonly referred to as the "top."

- The end opposite the top is known as the "base."

This ordering principle is sometimes called LIFO,last-in first-out.

# Queue

- A queue is an ordered collection of items where the addition of new items happens at one end, called the "rear", 
and the removal of existing items occurs at the other end, commonly called the "front."

- As an element enters the queue it starts at the rear and makes its way toward the front, 
waiting until that time when it is the next element to be removed.

**FIFO**

## Deque

- A deque, also known as a double-ended queue, is an ordered collection of items similar to the queue.

- It has two ends, a front and a rear, and the items remain positioned in the collection.