# 单例模式
class Singleton(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            orig = super(Singleton, cls)
            cls._instance = orig.__new__(cls, *args, **kwargs)
        return cls._instance


class MyClass(Singleton):
    a = 1


# 共享属性
class Borg(object):
    _state = {}

    def __new__(cls, *args, **kwargs):
        ob = super(Borg, cls).__new__(cls, *args, **kwargs)
        ob.__dict__ = cls._state
        return ob


class MyClass2(Borg):
    a = 1


def string_reverse2(text='abcdef'):
    new_text = list(text)
    new_text.reverse()
    return ''.join(new_text)


print(string_reverse2())


def string_reverse3(text='abcdef'):
    new_text = []
    for i in range(1, len(text)+1):
        new_text.append(text[-i])

    return ''.join(new_text)

from collections import deque


def string_reverse5(text='abcdef'):
    d = deque()
    d.extendleft(text)
    return ''.join(d)


list1 = [2, 3, 8, 4, 9, 5, 6]
list2 = [2, 3, 8, 4, 9, 5, 6]

list3 = list1 + list2
print(set(list3))

list_list = [x for x in range(1, 100) if x % 2 == 0]
print(list_list)

keys = ['Name', 'Sex', 'Age']
values = ['Jack', 'Male', 23]

for zip_i in zip(keys, values):
    print(zip_i)
dict_test = dict(zip(keys, values))

print(dict_test)


def yield_test(n):
    for i in range(n):
        yield call(i)
        print("i=", i)
    # 做一些其它的事情
    print("do something")
    print("end.")


def call(i):
    return i*2


for i in yield_test(5):
    print(i, ",")


i_list = [i for i in range(10)]
print(i_list)


def h():
    print("Wen Chuan")
    m = yield 5
    # print(m)
    yield m
    d = yield 12
    print("We are together!")


print("="*99)

# c = h()
# print(next(c))
# print(c.send('Fighting'))

for h in h():
    print(h)


def f():
    print('start')
    a = yield 1
    print(a)
    print('middle......')
    b = yield 2
    print(b)
    print('next')
    c = yield 3
    print(c)


a = f()
next(a)
next(a)
a.send('msg')

l1 = {'a','b','c','d','a','d','e','e'}
l2 = {}.fromkeys(l1).keys()
print(l2)

l1 = ['a', 'b', 'c', 'a', 'b']
l2 = []
list_list = [l2.append(i) for i in l1 if not i in l2]
print(list_list)


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def swapPairs(self, head):
        if head != None and head.next != None:
            next = head.next
            head.next = self.swapPairs(next.next)
            next.next = head
            return next
        return head


def binarySearch(l, t):
    low, high = 0, len(l)-1
    while low < high:
        print(low, high)
        mid = (low + high)/2
        if l[mid] > t:
            high = mid
        elif l[mid] < t:
            low= mid + 1
        else:
            return mid
    return low if l[low] == t else False


def qsort(seq):
    if seq == []:
        return []
    else:
        pivot = seq[0]
        lesser = qsort([x for x in seq[1:] if x < pivot])
        greater = qsort([x for x in seq[1:] if x >= pivot])
        return lesser+[pivot]+greater


class Node(object):
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right


tree = Node(1, Node(3, Node(7, Node(0))))


def lookup(root):
    stack = [root]
    while stack:
        current = stack.pop(0)
        print(current.data)
        if current.left:
            stack.append(current.left)
        if current.right:
            stack.append(current.right)


def deep(root):
    if not root:
        return
    print(root.data)
    deep(root.left)
    deep(root.right)